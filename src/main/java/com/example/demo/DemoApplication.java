package com.example.demo;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		String inputDirPath = "C:\\Users\\yijin\\Downloads\\PGSoft_Slots\\PG_SOFT";
		String outputDirPath = "C:\\Users\\yijin\\Test\\croppedImages";

		double widthRatio = 3.0;
		double heightRatio = 2.0;

		File inputDir = new File(inputDirPath);
		File outputDir = new File(outputDirPath);

		if(!outputDir.exists()) {
			outputDir.mkdir();
		}

		File[] imageFiles = inputDir.listFiles((dir, name) -> {
			return name.toLowerCase().endsWith(".jpg") || name.toLowerCase().endsWith(".jpeg") || name.toLowerCase().endsWith(".png");
		});

		if (imageFiles != null && imageFiles.length > 0) {
			for (File imageFile : imageFiles) {
				String outputFilePath = outputDirPath + "\\" + imageFile.getName();
				cropImage(imageFile.getAbsolutePath(), widthRatio, heightRatio, outputFilePath);
			}
		} else {
			System.out.println("No image files found in the directory.");
		}

//		cropImage("C:\\Users\\yijin\\Downloads\\PGSoft_Slots\\PG_SOFT\\diner-delight_icon_1024_square.png",3.0,2.0,"C:\\Users\\yijin\\Test\\croppedImage.png");
//		cropImage("C:\\Users\\User\\Pictures\\crop example\\16-9.jpg",3.0,2.0,"C:\\Users\\User\\Pictures\\test crop\\16-9-crop.jpg");

	}

	static void cropImage(String pathGetImage, double widthRatio, double heightRatio, String pathStore){
		try {
			BufferedImage originalImage = ImageIO.read(new File(pathGetImage));

			double originalRatio = (double) originalImage.getWidth() / (double) originalImage.getHeight();
			double targetRatio = widthRatio / heightRatio;

			int x, y, width, height;
			if (originalRatio >= targetRatio) { // image is wider than 2:3
				width = (int) Math.round(originalImage.getHeight() * targetRatio);
				height = originalImage.getHeight();
				x = (originalImage.getWidth() - width) / 2;
				y = 0;
			} else { // image is taller than 2:3
				width = originalImage.getWidth();
				height = (int) Math.round(originalImage.getWidth() / targetRatio);
				x = 0;
				y = (originalImage.getHeight() - height) / 2;
			}

			BufferedImage subImage = originalImage.getSubimage(x, y, width, height);
			System.out.println("Cropped Image Dimension: " + subImage.getWidth() + "x" + subImage.getHeight());

			File outputfile = new File(pathStore);
			ImageIO.write(subImage, "png", outputfile);
			System.out.println("Image cropped successfully: " + outputfile.getPath());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
